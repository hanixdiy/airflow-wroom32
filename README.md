# airflow-wroom32

This repository contains a sketch to interface the home made airflow sensor with a Wroom32 processor, initially using the Huzzah32 board.

* https://hanixdiy.blogspot.com/2014/08/build-airflow-sensor.html
* https://hanixdiy.blogspot.com/2014/10/airflow-sensor-test.html