// airflow project - prototype
// This sketch is to interface the airflow home made sensor to a Wroom32 processor
// (as Huzzah32 board)
//
// Button A N/A
// Button B N/A
// Button C N/A
//
// Pin used
// GPIOx  airflow input
// GPIOxx 18S20 input
//
// Author : TheFwGuy
// ChangeLog :
//    1.0
//    Basic I/O and display management

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);
 
// OLED FeatherWing buttons map to different pins depending on board:
#if defined(ESP8266)
  #define BUTTON_A  0
  #define BUTTON_B 16
  #define BUTTON_C  2
#elif defined(ESP32)
  #define BUTTON_A 15
  #define BUTTON_B 32
  #define BUTTON_C 14
#elif defined(ARDUINO_STM32_FEATHER)
  #define BUTTON_A PA15
  #define BUTTON_B PC7
  #define BUTTON_C PC5
#elif defined(TEENSYDUINO)
  #define BUTTON_A  4
  #define BUTTON_B  3
  #define BUTTON_C  8
#elif defined(ARDUINO_FEATHER52832)
  #define BUTTON_A 31
  #define BUTTON_B 30
  #define BUTTON_C 27
#else // 32u4, M0, M4, nrf52840 and 328p
  #define BUTTON_A  9
  #define BUTTON_B  6
  #define BUTTON_C  5
#endif

#define AIRFLOW_IN  15

#define ST_IDLE  0
#define ST_WAIT  1

#define PRESCALER_VALUE 60

int Status;
float BatteryLevel;

int TotalTimerCounter;    // Contains the number of seconds from the start
int RPMTiming = PRESCALER_VALUE;
uint8_t rawRPMCounter;    // raw counter - incremented under interrupt
uint8_t showRPMCounter;   // Contains the RPM count to show
uint8_t showRPMFlag;
uint8_t NumMagnets = 1;
uint8_t AcqSecTime = 1;

volatile int OnTimerInterruptCounter = 0;
volatile int OnAirflowToggleInterruptCounter = 0;


// ---- Timer management ------------
 
hw_timer_t * timer = NULL;
portMUX_TYPE Mux = portMUX_INITIALIZER_UNLOCKED;

// Handler for the I/O interrupt coming from the airflow sensor
void IRAM_ATTR onAirFlowInterrupt() {
  portENTER_CRITICAL_ISR(&Mux);
  OnAirflowToggleInterruptCounter++;
  portEXIT_CRITICAL_ISR(&Mux);
}

// Handler for timer interrupt 
void IRAM_ATTR onTimer() 
{
   portENTER_CRITICAL_ISR(&Mux);
   OnTimerInterruptCounter++;
   portEXIT_CRITICAL_ISR(&Mux);
}

void setup() 
{
   Serial.begin(9600);
 
   Serial.println("\n\nAirflow test 1.0");

   initDisplay();
    
   pinMode(BUTTON_A, INPUT_PULLUP);
   pinMode(BUTTON_B, INPUT_PULLUP);
   pinMode(BUTTON_C, INPUT_PULLUP);

   pinMode(AIRFLOW_IN, INPUT_PULLUP);
   attachInterrupt(digitalPinToInterrupt(AIRFLOW_IN), onAirFlowInterrupt, RISING);

   rawRPMCounter = 0;
   showRPMCounter = 0;
   showRPMFlag = 0;

   NumMagnets = 1;
   AcqSecTime = 1;

   Status = ST_IDLE;
   
   // Setup timer - interrupt will fire every second
   timer = timerBegin(0, 80, true);
   timerAttachInterrupt(timer, &onTimer, true);
   timerAlarmWrite(timer, 1000000, true);
   timerAlarmEnable(timer);
   
   TotalTimerCounter = 0;
   Serial.println("Timer initialized and started");

   display.setCursor(0,0);
   display.println("Airflow Test 1.0");
   display.display(); // actually display all of the above
}

void loop() 
{
   // Handling airflow interrupt
   if (OnAirflowToggleInterruptCounter > 0)
   {
      // Airflow interrupt fired
      portENTER_CRITICAL(&Mux);
      OnAirflowToggleInterruptCounter--;
      portEXIT_CRITICAL(&Mux);

      // Consider the reading correct only if is not a spike - a single reading should do
      if(digitalRead(AIRFLOW_IN))
         rawRPMCounter++;
   }

   // Handling timer
   if (OnTimerInterruptCounter > 0)
   {
      // Timer expired - reset it - restart
      portENTER_CRITICAL(&Mux);
      OnTimerInterruptCounter--;
      portEXIT_CRITICAL(&Mux);
 
      TotalTimerCounter++;    // Increment number of seconds

      showRPMCounter = rawRPMCounter;
      rawRPMCounter = 0;
      showRPMFlag = 1;      
   }

   displayRPM();

   switch(Status)
   {
      case ST_IDLE:
         if(readButton(BUTTON_A)) 
         {
         }

         if(readButton(BUTTON_B)) 
         {
         }

         if(readButton(BUTTON_C)) 
         {
         }
         break;
   }
  

   delay(10);
   yield();
   display.display();
}

// The function read a button with some debouncing - return true if the button is pressed and then released.
// returns false otherwise
boolean readButton(int button)
{
   if(!digitalRead(button))
   {
      delay(20);
      if(!digitalRead(button))
      {
         while(!digitalRead(button));
         delay(10);
         return true;
      }
   }
   return false;
}

// Init display
// The function set up the display and leave it clean and ready
// Function needed only once
void initDisplay()
{
   // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally  
   display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
   // Show image buffer on the display hardware.
   // Since the buffer is intialized with an Adafruit splashscreen
   // internally, this will display the splashscreen.
   display.display();
   delay(1000);
 
   // text display tests
   display.setTextSize(1);
   display.setTextColor(WHITE);
   display.setCursor(0,0);

   // Clear the buffer.
   display.clearDisplay();
   display.display();

}


// Reset display
// The function set up the display and leave it clean and ready
void resetDisplay()
{
   // text display tests
   display.setTextSize(1);
   display.setTextColor(WHITE);
   display.setCursor(0,0);

   // Clear the buffer.
   display.clearDisplay();
   display.display();
}

// Display Counter
void displayRPM()
{
   int showRealRPM = (showRPMCounter / NumMagnets) * (60 / AcqSecTime);
   
   if (showRPMFlag == 1) 
   {
      // text display tests
      display.setTextSize(4);
      display.setTextColor(WHITE);
      display.setCursor(0,0);

      // Clear the buffer.
      display.clearDisplay();
      display.print(showRealRPM);    
      display.display();
      showRPMFlag = 0;
   }
}

